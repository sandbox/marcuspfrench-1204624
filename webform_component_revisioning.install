<?php
// $Id

/**
 * Implementation of hook_schema().
 * Defines the database table structure for storing component revisions.
 */
function webform_component_revisioning_schema() {
	$schema = array();

	$schema['component_revisions'] = array(
		'description' => 'Stores information about components for webform nodes.',
		'fields' => array(
			'nid' => array(
				'description' => 'The node identifier of a webform.',
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'cid' => array(
				'description' => 'The identifier for this component within this node, starts at 0 for each node.',
				'type' => 'int',
				'size' => 'small',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'vid' => array(
				'description' => 'The identifier for the node revision this component is part of.',
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'pid' => array(
				'description' => 'If this component has a parent fieldset, the cid of that component.',
				'type' => 'int',
				'size' => 'small',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'form_key' => array(
				'description' => 'When the form is displayed and processed, this key can be used to reference the results.',
				'type' => 'varchar',
				'length' => 128,
			),
			'name' => array(
				'description' => 'The label for this component.',
				'type' => 'text',
			),
			'type' => array(
				'description' => 'The field type of this component (textfield, select, hidden, etc.).',
				'type' => 'varchar',
				'length' => 16,
			),
			'value' => array(
				'description' => 'The default value of the component when displayed to the end-user.',
				'type' => 'text',
				'not null' => TRUE,
			),
			'extra' => array(
				'description' => 'Additional information unique to the display or processing of this component.',
				'type' => 'text',
				'not null' => TRUE,
			),
			'mandatory' => array(
				'description' => 'Boolean flag for if this component is required.',
				'type' => 'int',
				'size' => 'tiny',
				'not null' => TRUE,
				'default' => 0,
			),
			'weight' => array(
				'description' => 'Determines the position of this component in the form.',
				'type' => 'int',
				'size' => 'small',
				'not null' => TRUE,
				'default' => 0,
			),
		),
		'primary key' => array('nid', 'vid', 'cid'),
	);
  
	return $schema;
}

/**
 * Increase the size of the component instance name in case the unlimited_component_name module is installed.
 */
function webform_component_revisioning_update_6101() {
  $ret = array();
  db_change_field($ret, 'component_revisions', 'name', 'name', array('type' => 'text'));
  return $ret;
}

/**
 * Implementation of hook_install().
 * Creates the database table defined in the schema hook.
*/
function webform_component_revisioning_install() {
	drupal_install_schema('webform_component_revisioning');
	db_query("UPDATE {system} SET weight = 1 WHERE name='webform_component_revisioning' AND type='module'");
}

/**
 * Implementation of hook_enable().
 * Fills the database table with component revisions 
 * for all existing components that don't have a revision.
 */
function webform_component_revisioning_enable() {
	$revisions = db_query('SELECT vid, nid FROM {node_revisions}');
	while ($revision = db_fetch_array($revisions)) {
		$components = db_query('SELECT * FROM {webform_component} WHERE nid = %d', $revision['nid']);
		$hasrevisions = db_fetch_array(db_query('SELECT * FROM {component_revisions} WHERE vid = %d', $revision['vid']));
		if (empty($hasrevisions)) {
			db_lock_table('component_revisions');
			while ($component = db_fetch_array($components)) {
				$component['value'] = isset($component['value']) ? $component['value'] : NULL;
				$component['mandatory'] = isset($component['mandatory']) ? $component['mandatory'] : 0;
				db_query("INSERT INTO {component_revisions} (vid, nid, cid, pid, form_key, name, type, value, extra, mandatory, weight) VALUES (%d, %d, %d, %d, '%s', '%s', '%s', '%s', '%s', %d, %d)", $revision['vid'], $component['nid'], $component['cid'], $component['pid'], $component['form_key'], $component['name'], $component['type'], $component['value'], $component['extra'], $component['mandatory'], $component['weight']);
			}
		}
		db_unlock_tables();
	}
}

/**
 * Implementation of hook_uninstall().
 * Deletes the database table defined in the schema hook.
 */
function webform_component_revisioning_uninstall() {
	drupal_uninstall_schema('webform_component_revisioning');
}